using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedicineManager : Singleton<MedicineManager>
{

    public int lives = 3;

    public int antiBioticsCount;
    public int benadrylCount;
    public int aspirinCount;

    //bool roundEnded = false;

    public Button giveButton;
    public GameObject submitButton;

    public int antiBioticsRequested;
    public int benadrylRequested;
    public int aspirinRequested;


    private void Start()
    {
        Generator();
        giveButton.onClick.AddListener(TaskOnClick);


        //respawner = FindObjectOfType<Respawner>();
    }


    //more help from annalise

    private void Generator()
    {
        antiBioticsRequested = GameManager.instance.medicineCount[0];
        benadrylRequested = GameManager.instance.medicineCount[1];
        aspirinRequested = GameManager.instance.medicineCount[2];

        //Debug.Log("Anti Biotics needed = " + antiBioticsRequested);
        //Debug.Log("Benadryl needed = " + benadrylRequested);
        //Debug.Log("Aspirin needed = " + aspirinRequested);

    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.name == "Anti Biotics" || other.gameObject.name == "Anti Biotics(Clone)")
        {
            antiBioticsCount++;
        }

        if (other.gameObject.name == "Benadryl" || other.gameObject.name == "Benadryl(Clone)")
        {
            benadrylCount ++;
        }

        if (other.gameObject.name == "Aspirin" || other.gameObject.name == "Aspirin(Clone)")
        {
            aspirinCount++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Anti Biotics" || other.gameObject.name == "Anti Biotics(Clone)")
        {
            antiBioticsCount--;
        }

        if (other.gameObject.name == "Benadryl" || other.gameObject.name == "Benadryl(Clone)")
        {
            benadrylCount--;
        }

        if (other.gameObject.name == "Aspirin" || other.gameObject.name == "Aspirin(Clone)")
        {
            aspirinCount--;
        }
    }

    //until here I think

    private void TaskOnClick()
    {

        if (antiBioticsCount == antiBioticsRequested && benadrylCount == benadrylRequested)
        {

            EventManager.TriggerEvent(Events.RoundEnd);
            submitButton.SetActive(false);
            
        }
        else
        {
            EventManager.TriggerEvent(Events.Fail);
        }
    }

    void OnCleanUp()
    {
            antiBioticsCount = 0;
            benadrylCount = 0;
            aspirinCount = 0;
    }

    void OnEnable()
    {
        EventManager.StartListening(Events.RoundStart, OnRoundStart);
        EventManager.StartListening(Events.RoundEnd, OnRoundEnd);
        EventManager.StartListening(Events.RoundMid, DuringRound);
        EventManager.StartListening(Events.CleanUp, OnCleanUp);
    }

    void OnRoundStart()
    {
        Generator();  
    }

    void DuringRound()
    {
        submitButton.SetActive(true);
    }

    void OnRoundEnd()
    {

        if(antiBioticsCount == 1)
        {     
            Destroy(GameObject.Find("Anti Biotics"));
            Destroy(GameObject.Find("Anti Biotics(Clone)"));
            antiBioticsCount = 0;
        }
        if(benadrylCount == 1)
        {
            Destroy(GameObject.Find("Benadryl"));
            Destroy(GameObject.Find("Benadryl(Clone)"));
            benadrylCount = 0;
        }
        if (aspirinCount == 1)
        {
            Destroy(GameObject.Find("Aspirin"));
            Destroy(GameObject.Find("Aspirin(Clone)"));
            aspirinCount = 0;
        }



    }

}

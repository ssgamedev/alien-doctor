using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Respawner : MonoBehaviour
{

    public Button cleanupButton;

    public GameObject antiBiotics;
    public GameObject benadryl;
    public GameObject aspirin;
    GameObject medicine;
    
    public GameObject benadrylPrefab;
    public GameObject antiBioticsPrefab;
    public GameObject aspirinPrefab;
    
    float xCoordinate;

    int antiBioticsInScene;
    int benadrylInScene;
    int aspirinInScene;

    private void Start()
    {
        InvokeRepeating("Counter", 0f, 1);
        Button btn = cleanupButton.GetComponent<Button>();
        btn.onClick.AddListener(CleanUp);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == antiBiotics) 
        {
            Destroy(other.gameObject);
          
        }

        if (other.gameObject == benadryl)
        {
            Destroy(other.gameObject);
        }

        if (other.gameObject == aspirin)
        {
            Destroy(other.gameObject);
        }
    }

    public void Counter()
    {
        antiBioticsInScene = FindObjectsOfType<antiBioticsID>().Length;
        benadrylInScene = FindObjectsOfType<benadrylID>().Length;
        aspirinInScene = FindObjectsOfType<aspirinID>().Length;

        if (antiBioticsInScene < 1)
        {
            xCoordinate = -0.75f;
            Respawn(antiBioticsPrefab);
            antiBiotics = medicine;
        }

        if (benadrylInScene < 1)
        {
            xCoordinate = -0.5f;
            Respawn(benadrylPrefab);
            benadryl = medicine;
        }

        if (aspirinInScene < 1)
        {
            xCoordinate = -0.25f;
            Respawn(aspirinPrefab);
            aspirin = medicine;
        }
    }

    //Got help from annalise for this function

    public void Respawn(GameObject medicineRespawn)
    {
        GameObject tempMedicine = Instantiate(medicineRespawn, new Vector3(xCoordinate, 2f, -9f), Quaternion.identity);
        medicine = tempMedicine; 
    }

    private void CleanUp()
    {
        Destroy(GameObject.Find("Aspirin"));
        Destroy(GameObject.Find("Aspirin(Clone)"));
        Destroy(GameObject.Find("Benadryl"));
        Destroy(GameObject.Find("Benadryl(Clone)"));
        Destroy(GameObject.Find("Anti Biotics"));
        Destroy(GameObject.Find("Anti Biotics(Clone)"));
        EventManager.TriggerEvent(Events.CleanUp);
    }
}

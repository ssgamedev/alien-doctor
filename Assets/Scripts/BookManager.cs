using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookManager : MonoBehaviour
{
    private Camera mainCamera;
    public GameObject bookFront;
    public GameObject bookBack;
    public GameObject speechBubble;

    public float speed = 1f;

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        //got this from a unity thread i think
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "BookFront")
                {
                    BookOpen();
                }

                if (hit.transform.name == "BookBack")
                {
                    BookClose();
                }
            }
        }
    }

    void BookOpen()
    {
        bookFront.transform.position = new Vector3(0.171f, 1.107f, -9.556f);
        bookBack.transform.position = new Vector3(-0.162f, 1.104f, -9.549f);
        speechBubble.SetActive(false);
        bookFront.transform.rotation = Quaternion.Euler(-2.594f, 457.15f, -70.162f);
        bookBack.transform.rotation = Quaternion.Euler(-1.564f, 625, -109.9f);
    }

    void BookClose()
    {
        bookFront.transform.position = new Vector3(-0.45f, 0.445f, -9.45f);
        bookBack.transform.position = new Vector3(-0.45f, 0.44f, -9.44f);
        speechBubble.SetActive(true);
        bookFront.transform.rotation = Quaternion.Euler(0, 630, -110f);
        bookBack.transform.rotation = Quaternion.Euler(0, 630, -110f);
    }

}

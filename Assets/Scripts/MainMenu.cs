using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    //I'm certain there's a better way to do this but I couldn't think of anything and it's too late for me to bother.

    public Button startButton;
    public Button helpButton;
    public Button quitButton;

    public int buttonID;

    public Button helpExitButton;
    public Button helpHelpExitButton;
    public Button medicineButton;
    public Button journalButton;
    public Button basketButton;
    public Button giveHelpButton;
    public Button speechButton;
    public Button timerButton;
    public Button cleanUpButton;
    public Button scoreButton;

    public GameObject helpHelpExitObj;

    bool menuUp;

    public GameObject medicineText;
    public GameObject journalText;
    public GameObject basketText;
    public GameObject giveHelpText;
    public GameObject speechText;
    public GameObject timerText;
    public GameObject cleanUpText;
    public GameObject scoreText;

    public GameObject helpPanel;

    private void Start()
    {
        startButton.onClick.AddListener(StartGame);
        helpButton.onClick.AddListener(HelpScreen);
        quitButton.onClick.AddListener(QuitGame);

        helpExitButton.onClick.AddListener(() => HelpButtonClicked(helpExitButton));
        helpHelpExitButton.onClick.AddListener(() => HelpButtonClicked(helpHelpExitButton));
        medicineButton.onClick.AddListener(() => HelpButtonClicked(medicineButton));
        journalButton.onClick.AddListener(() => HelpButtonClicked(journalButton));
        basketButton.onClick.AddListener(() => HelpButtonClicked(basketButton));
        giveHelpButton.onClick.AddListener(() => HelpButtonClicked(giveHelpButton));
        speechButton.onClick.AddListener(() => HelpButtonClicked(speechButton));
        timerButton.onClick.AddListener(() => HelpButtonClicked(timerButton));
        cleanUpButton.onClick.AddListener(() => HelpButtonClicked(cleanUpButton));
        scoreButton.onClick.AddListener(() => HelpButtonClicked(scoreButton));
    }

    private void StartGame()
    {
        SceneManager.LoadScene("MainScene");
    }

    private void HelpScreen()
    {
        helpPanel.SetActive(true);
    }

    private void QuitGame()
    {
        Application.Quit();
    }

    private void HelpButtonClicked(Button buttonPressed)
    {
        if (buttonPressed == helpExitButton && menuUp == false)
        {
            helpPanel.SetActive(false);
            helpHelpExitObj.SetActive(false);
        }

        if (buttonPressed == helpHelpExitButton)
        {
            helpHelpExitObj.SetActive(false);
            menuUp = false;

            medicineText.SetActive(false);
            journalText.SetActive(false);
            basketText.SetActive(false);
            giveHelpText.SetActive(false);
            speechText.SetActive(false);
            timerText.SetActive(false);
            cleanUpText.SetActive(false);
            scoreText.SetActive(false);            
        }

        if (buttonPressed == medicineButton && menuUp == false)
        {
            TextActivator(medicineText);
        }
        if (buttonPressed == journalButton && menuUp == false)
        {
            TextActivator(journalText);
        }
        if (buttonPressed == basketButton && menuUp == false)
        {
            TextActivator(basketText);
        }
        if (buttonPressed == giveHelpButton && menuUp == false)
        {
            TextActivator(giveHelpText);
        }
        if (buttonPressed == speechButton && menuUp == false)
        {
            TextActivator(speechText);
        }
        if (buttonPressed == timerButton && menuUp == false)
        {
            TextActivator(timerText);
        }
        if (buttonPressed == cleanUpButton && menuUp == false)
        {
            TextActivator(cleanUpText);
        }
        if (buttonPressed == scoreButton && menuUp == false)
        {
            TextActivator(scoreText);
        }
    }

    void TextActivator(GameObject activeText)
    {
        helpHelpExitObj.SetActive(true);
        activeText.SetActive(true);
        menuUp = true;
    }

}

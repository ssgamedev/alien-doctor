using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float timer = 300;
    public TMPro.TextMeshProUGUI timeText;
    public GameObject addTime;
    public GameObject subtractTime;

    private void OnEnable()
    {
        EventManager.StartListening(Events.RoundEnd, AddTime);
        EventManager.StartListening(Events.Fail, SubtractTime);
    }

    void AddTime()
    {
        timer += 30;
        FindObjectOfType<AudioManager>().Play("TimeUp");
        addTime.SetActive(true);
        Invoke("UiClear", 1f);
    }

    void SubtractTime()
    {
        timer -= 60;
        FindObjectOfType<AudioManager>().Play("TimeDown");
        subtractTime.SetActive(true);
        Invoke("UiClear", 1f);
    }

    void UiClear()
    {
        addTime.SetActive(false);
        subtractTime.SetActive(false);
    }

    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 0;
            EventManager.TriggerEvent(Events.GameEnd);
        }
        TimeDisplay(timer);
    }

    void TimeDisplay(float timeToDisplay)
    {
        if(timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }

        //Got this from some unity thread

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliseconds = Mathf.FloorToInt(timeToDisplay * 1000f) % 1000;

        timeText.text = string.Format("time left: {0:0}:{1:00}:{2:000}", minutes, seconds, milliseconds);

    }
}

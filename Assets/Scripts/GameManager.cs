using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : Singleton<GameManager>
{

    public int score;
    float highScore;

    public GameObject gameOverPanel;
    public GameObject speechBubble;
    public GameObject environmentSound;
    public GameObject newHighScore;

    public TMPro.TextMeshProUGUI highScoreText;
    public TMPro.TextMeshProUGUI scoreDisplay;
    public TMPro.TextMeshProUGUI alienText;

    public Button restartButton;
    public Button mainMenuButton;

    int benadrylSeed;
    int antiBioticsSeed;
    int aspirinSeed;
    int textOrderSeed;

    string benadrylText;
    string antiBioticsText;
    string aspirinText;


    //Got help from Annalise for this bit

    public List<int> medicineCount = new List<int>()
    {
        0, //benadryl
        0, //antibiotics
        0, //aspirin
    };

    //to here

    private void Start()
    {
        highScore = PlayerPrefs.GetFloat("highScore");
        gameOverPanel.SetActive(false);
        OnRoundStart();

        restartButton.onClick.AddListener(ReloadScene);
        mainMenuButton.onClick.AddListener(ToMainMenu);

    }

    public void MedicineRequested()
    {
        for (int i = 0; i < medicineCount.Count; i++)
        {
            medicineCount[i] = Random.Range(0, 2);
        }
        
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.RoundStart, OnRoundStart);
        EventManager.StartListening(Events.RoundEnd, OnRoundEnd);
        EventManager.StartListening(Events.RoundMid, DuringRound);
        EventManager.StartListening(Events.GameEnd, OnGameEnd);
    }

    void OnRoundStart()
    {
        Debug.Log("Round Started");  
        MedicineRequested();
    }

    void DuringRound()
    {
        speechBubble.SetActive(true);
        TextHandler();
        
    }
    void OnRoundEnd()
    {
        speechBubble.SetActive(false);
        score += 1;
        scoreDisplay.text = "score: " + score;
    }

    void TextHandler()
    {

        benadrylSeed = Random.Range(0, 5);
        antiBioticsSeed = Random.Range(0, 5);
        aspirinSeed = Random.Range(0, 5);
        textOrderSeed = Random.Range(0, 6);

        if (medicineCount[0] == 0)
        {

            //No Symptoms

            switch (benadrylSeed)
            {
                case 1:
                    benadrylText = "My eyes are fine";
                    break;
                case 2:
                    benadrylText = "My eyes are great";
                    break;
                case 3:
                    benadrylText = "I can't complain about my eyes";
                    break;
                case 4:
                    benadrylText = "My eyes don't hurt";
                    break;
                default:
                    benadrylText = "";
                    break;

            }
        }

        if (medicineCount[1] == 0)
        {

            switch (antiBioticsSeed)
            {
                case 1:
                    antiBioticsText = "My legs are fine";
                    break;
                case 2:
                    antiBioticsText = "My legs are great";
                    break;
                case 3:
                    antiBioticsText = "I can't complain about my legs";
                    break;
                case 4:
                    antiBioticsText = "My legs don't hurt";
                    break;
                default:
                    antiBioticsText = "";
                    break;

            }
        }

        if (medicineCount[2] == 0)
        {

            switch (aspirinSeed)
            {
                case 1:
                    aspirinText = "My nose is fine";
                    break;
                case 2:
                    aspirinText = "My nose is great";
                    break;
                case 3:
                    aspirinText = "I can't complain about my nose";
                    break;
                case 4:
                    aspirinText = "My nose doesn't hurt";
                    break;
                default:
                    aspirinText = "";
                    break;

            }
        }

        //Negative Symptoms

        if (medicineCount[0] == 1)
        {

            switch (benadrylSeed)
            {
                case 1:
                    benadrylText = "My eyes are killing me";
                    break;
                case 2:
                    benadrylText = "My eyes are sore";
                    break;
                case 3:
                    benadrylText = "My eyes really hurt";
                    break;
                case 4:
                    benadrylText = "My eyes don't not hurt";
                    break;
                default:
                    benadrylText = "My eyes hurt";
                    break;

            }
        }

        if (medicineCount[1] == 1)
        {

            switch (antiBioticsSeed)
            {
                case 1:
                    antiBioticsText = "My legs are killing me";
                    break;
                case 2:
                    antiBioticsText = "My legs are sore";
                    break;
                case 3:
                    antiBioticsText = "My legs really hurt";
                    break;
                case 4:
                    antiBioticsText = "My legs don't not hurt";
                    break;
                default:
                    antiBioticsText = "My legs hurt";
                    break;

            }
        }

        if (medicineCount[2] == 1)
        {

            switch (aspirinSeed)
            {
                case 1:
                    aspirinText = "My nose is killing me";
                    break;
                case 2:
                    aspirinText = "My nose is sore";
                    break;
                case 3:
                    aspirinText = "My nose really hurts";
                    break;
                case 4:
                    aspirinText = "My nose doesn't not hurt";
                    break;
                default:
                    aspirinText = "My nose hurts";
                    break;

            }
        }

        switch (textOrderSeed)
        {
            case 1:
                alienText.text = benadrylText + "\n" + antiBioticsText + "\n" + aspirinText;
                break;
            case 2:
                alienText.text = antiBioticsText + "\n" + benadrylText + "\n" + aspirinText;
                break;
            case 3:
                alienText.text = aspirinText + "\n" + antiBioticsText + "\n" + benadrylText;
                break;
            case 4:
                alienText.text = benadrylText + "\n" + aspirinText + "\n" + antiBioticsText;
                break;
            case 5:
                alienText.text = antiBioticsText + "\n" + aspirinText + "\n" + benadrylText;
                break;
            default:
                alienText.text = aspirinText + "\n" + benadrylText + "\n" + antiBioticsText;
                break;

        }

    }


    void OnGameEnd()
    {
        speechBubble.SetActive(false);
        gameOverPanel.SetActive(true);
        environmentSound.SetActive(false);
        if (highScore < score)
        {
            newHighScore.SetActive(true);
            PlayerPrefs.SetFloat("highScore", score);
            highScore = score;
        }
        highScoreText.text = "high score: " + highScore;
        
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


}

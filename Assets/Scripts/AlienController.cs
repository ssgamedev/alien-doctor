using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienController : MonoBehaviour
{
    Animator anim;
    public GameObject alien;
    int colourSeed;




    private void Start()
    {
        alien.SetActive(true);
        anim = GetComponent<Animator>();
        OnRoundStart();
    }

    void OnEnable()
    {
        EventManager.StartListening(Events.RoundStart, OnRoundStart);
        EventManager.StartListening(Events.RoundEnd, OnRoundEnd);
    }

    void OnRoundStart()
    {
        colourSeed = Random.Range(0, 6);
        alien.SetActive(true);
        StartCoroutine(RoundStart());
        switch (colourSeed)
        {
            case (1):
                alien.GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case (2):
                alien.GetComponent<Renderer>().material.color = Color.blue;
                break;
            case (3):
                alien.GetComponent<Renderer>().material.color = Color.cyan;
                break;
            case (4):
                alien.GetComponent<Renderer>().material.color = Color.grey;
                break;
            case (5):
                alien.GetComponent<Renderer>().material.color = Color.magenta;
                break;
            default:
                alien.GetComponent<Renderer>().material.color = Color.green;
                break;
        }

    }


    void OnRoundEnd()
    {
        FindObjectOfType<AudioManager>().Play("AlienEat");
        StartCoroutine(Exit());
    }

    IEnumerator RoundStart()
    {
        yield return new WaitForSeconds(2);
        //transform.position = new Vector3(0, 0.515f, -7.72f);
        StartCoroutine(MidRound());
    }

    IEnumerator Exit()
    {
        anim.SetTrigger("Exit");
        yield return new WaitForSeconds(2);
        alien.transform.position = new Vector3(0, 0.515f, -7.72f);
        alien.transform.rotation = Quaternion.Euler(0, -90f, 0);
        alien.SetActive(false);
        //alien.SetActive(true);
        EventManager.TriggerEvent(Events.RoundStart);
    }

    IEnumerator MidRound()
    {
        FindObjectOfType<AudioManager>().Play("AlienSpeech");
        EventManager.TriggerEvent(Events.RoundMid);
        yield return null;
    }


}
